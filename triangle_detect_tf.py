import glob
import cv2
import numpy as np
import tensorflow as tf


# read data
def input(path, length = 0):
    if length == 0:
        length = len(glob.glob(path + '/*.png'))
    return [cv2.imread(filename, 0) for filename in glob.glob(path + '/*.png')[:length]]


# reduce noise
def low_pass_filter(data):
    kernel = np.ones((5, 5), np.float32) / 25
    return [cv2.filter2D(x, -1, kernel) for x in data]


# preprocess using laplacian
def laplacian(data):
    return [cv2.Laplacian(x, cv2.CV_64F) for x in data]


# convert image to matrix
def convert_to_matrix(data):
    return [np.reshape(img, 400, np.float32) for img in data]


# data path
non_triangle_data_path = 'triangle_competition/train/non-triangle'
triangle_data_path = 'triangle_competition/train/triangle'

# input data
print 'reading...'
triangle_data_original = input(triangle_data_path)
non_triangle_data_original = input(non_triangle_data_path, len(triangle_data_original)*5)


print 'preprocessing...'
# filter noise
triangle_data = low_pass_filter(triangle_data_original)
non_triangle_data = low_pass_filter(non_triangle_data_original)

# preprocess data
triangle_data = laplacian(triangle_data)
non_triangle_data = laplacian(non_triangle_data)

# convert image to matrix
triangle_data = convert_to_matrix(triangle_data)
non_triangle_data = convert_to_matrix(non_triangle_data)


batch_xs_temp = triangle_data + non_triangle_data
labels_temp = []
for i in range(len(triangle_data)):
    labels_temp.append(np.reshape([0, 1], (2,1)))
for i in range(len(non_triangle_data)):
    labels_temp.append(np.reshape([1, 0], (2,1)))
suff = [i for i in range(len(batch_xs_temp))]

np.random.shuffle(suff)
# print triangle_data[0]
batch_xs = [batch_xs_temp[i] for i in suff]
labels = [labels_temp[i] for i in suff]
train_idx, test_idx = batch_xs[:len(batch_xs) * 7 / 10] , batch_xs[len(batch_xs) * 7 / 10:]
train_labels, test_labels = labels[:len(batch_xs) * 7 / 10], labels[len(batch_xs) * 7 / 10:]
train_idx = np.reshape(train_idx, (len(train_idx), 400), np.float32)
test_idx = np.reshape(test_idx, (len(test_idx), 400), np.float32)
train_labels = np.reshape(train_labels, (len(train_labels), 2))
test_labels = np.reshape(test_labels, (len(test_labels), 2))

# using tensorflow
print 'training...'
x = tf.placeholder(tf.float32, [None, 400])

W = tf.Variable(tf.zeros([400,2]))
b = tf.Variable(tf.zeros([2]))

y = tf.nn.softmax(tf.matmul(x,W) + b)

y_ = tf.placeholder(tf.float32, [None, 2])

cost = -tf.reduce_sum(y_*tf.log(y))

train_step = tf.train.GradientDescentOptimizer(0.00000001).minimize(cost) #0.00000003

init = tf.initialize_all_variables()

sess = tf.Session()
sess.run(init)
for i in range(1000):
    sess.run(train_step, feed_dict={x: train_idx, y_:train_labels})
    print sess.run(cost, feed_dict={y_: train_labels, x: train_idx})

correct_prediction = tf.equal(tf.argmax(y,1), tf.argmax(y_,1))
accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))
print(sess.run(accuracy, feed_dict={x: test_idx, y_: test_labels}))

print 'predicting...'
f = open('triangle_detect_tensorflow_result.txt', 'w')
path = 'triangle_competition/test/*.png'
for file in glob.glob(path):
    img = cv2.imread(file, 0)
    img = convert_to_matrix(laplacian(low_pass_filter([img])))
    filename = file.split('/')[len(file.split('/')) - 1]
    f.write(str(sess.run(tf.argmax(sess.run(y, feed_dict={x:img}), 1))[0]) + " " + filename + '\n')
f.close()
sess.close()
