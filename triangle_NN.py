import numpy as np
import glob
import data as db

print 'reading...'
triangle_data = db.reader("triangle_competition/train/triangle")[0]
non_triangle_data = db.reader("triangle_competition/train/non-triangle", len(triangle_data) * 3)[0]


np.random.shuffle(triangle_data)
np.random.shuffle(non_triangle_data)

batch_xs_temp = triangle_data[:len(triangle_data) * 8 / 10] + non_triangle_data[:len(triangle_data) * 8 / 10]
labels_temp = []
for i in range(len(triangle_data) * 8 / 10):
    labels_temp.append(1)
for i in range(len(batch_xs_temp) - len(triangle_data) * 8 / 10):
    labels_temp.append(0)

suff = [i for i in range(len(batch_xs_temp))]
np.random.shuffle(suff)

train_idx = [batch_xs_temp[i] for i in suff]
y = [labels_temp[i] for i in suff]

triangle_validation_idx = triangle_data[len(triangle_data) * 8 / 10:]
non_triangle_validation_idx = non_triangle_data[len(triangle_data) * 8 / 10:]

triangle_validation_labels = []
non_triangle_validation_labels = []
for i in range(len(triangle_validation_idx)):
    triangle_validation_labels.append(1)
for j in range(len(non_triangle_validation_idx)):
    non_triangle_validation_labels.append(0)

# print type(y)

X = np.reshape(train_idx, (len(train_idx), 400), np.float32)
# Y = train_labels

triangle_validation_idx = np.reshape(triangle_validation_idx, (len(triangle_validation_idx), 400), np.float32)

non_triangle_validation_idx = np.reshape(non_triangle_validation_idx, (len(non_triangle_validation_idx), 400),
                                         np.float32)

print 'training...'

# initialize parameters
neural = 0
max = 0
# for h in range(2,400):
h = 150
D = 400
k = 2
W = 0.01 * np.random.randn(D, h)
b = np.zeros((1, h))
W2 = 0.01 * np.random.randn(h, k)
b2 = np.zeros((1, k))

size_step = 0.06
reg = 1e-2
num_examples = X.shape[0]
for i in xrange(10000):
    # forward
    hidden_layer = np.maximum(0, np.dot(X, W) + b)
    scores = np.dot(hidden_layer, W2) + b2

    # compute the class probabilities
    exp_scores = np.exp(scores)
    probs = exp_scores / np.sum(exp_scores, axis=1, keepdims=True)

    # compute the loss:
    correct_logprobs = -np.log(probs[range(num_examples), y])
    data_loss = np.sum(correct_logprobs) / num_examples
    reg_loss = 0.5 * reg * np.sum(W * W) + 0.5 * reg * np.sum(W2 * W2)
    loss = data_loss + reg_loss
    if i % 500 == 0:
        print "iteration %d: loss %f" % (i, loss)
        hidden_layer = np.maximum(0, np.dot(triangle_validation_idx, W) + b)
        scores = np.dot(hidden_layer, W2) + b2
        triangle_predicts = np.argmax(scores, axis=1)
        hidden_layer = np.maximum(0, np.dot(non_triangle_validation_idx, W) + b)
        scores = np.dot(hidden_layer, W2) + b2
        non_triangle_predicts = np.argmax(scores, axis=1)
        hidden_layer = np.maximum(0, np.dot(X, W) + b)
        scores = np.dot(hidden_layer, W2) + b2
        predict_class = np.argmax(scores, axis=1)
        mean_triangle = np.mean(triangle_predicts == triangle_validation_labels)
        mean_non_triangle = np.mean(non_triangle_predicts == non_triangle_validation_labels)
        P = mean_triangle * len(triangle_validation_idx) / (mean_triangle * len(triangle_validation_idx) + (1 - mean_non_triangle) * len(non_triangle_validation_idx))
        R = mean_triangle
        print predict_class
        print y
        print np.mean(predict_class == y)
        print 'training accuracy: %d / %d - ' % (np.mean(predict_class == y) * len(y), len(y)),
        print ' %.2f  -  %.2f' % (P, R)

    # gradient on scores
    dscores = probs
    dscores[range(num_examples), y] -= 1

    dscores /= num_examples
    # backpropate
    dW2 = np.dot(hidden_layer.T, dscores)
    db2 = np.sum(dscores, axis=0, keepdims=True)

    dhidden = np.dot(dscores, W2.T)

    dhidden[hidden_layer <= 0] = 0

    dW = np.dot(X.T, dhidden)
    db = np.sum(dhidden, axis=0, keepdims=True)

    dW2 += reg * W2
    dW += reg * W

    W += -size_step * dW
    b += -size_step * db
    W2 += -size_step * dW2
    b2 += -size_step * db2

# f = open('W.txt', 'w')
# g = open('W2.txt', 'w')
# f.write(str(W))
# f.write(str(b))
# g.write(str(W2))
# g.write(str(b2))
# f.close()
# g.close()
# hidden_layer = np.maximum(0, np.dot(X, W) + b)
# scores = np.dot(hidden_layer, W2) + b2
# predict_class = np.argmax(scores, axis=1)
# print predict_class
# print 'training accuracy: %.2f' % (np.mean(predict_class == y))
